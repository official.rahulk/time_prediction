# A model to predict the time required to deliver a package.
Created a Neural network regression model to predict time .
Used Tensorflow to implement NN with 5 hidden layers.


Here is the response to the requested questions:

## Your thought process while working on this assignment.
I was asked to predict the time required to deliver a package on the basis of few variables as 'destinationbranch', 'originbranch' and 'producttype'.
So this is clearly a regression problem. I need few predictors and targets to perform the regression.
I decided to perform regression using Neural Network with 5 hidden layers.

I was provided with some chunk of data which was a json file.

First I needed to understand the data to perform some feature engineering. I tried to open the json file but faced some issue since file was of 500MB and it was taking long time to process it.
Then i decided to use pandas library and process the data using it. I also used 'ijson' lib to efficiently handle the json file.

Once the file was open, I explored the data and found few insights as:
- Interestingly variable called 'detail' was having  possible data points which can be used to predict the delivery of the package.
- For variable 'detail', I extracted the date and time values from 1st (Delivery timestamp)
and the last (Dispatched timestamp) item from sub list. Used this variable to compute the overall_time taken to deliver the package.
- For variables 'destinationbranch' and 'originbranch' was having values as string. Since it is a regression problem and I need data points for them which needs to be integer, so i decided to use TFIDF to convert each string into unique numbers.
- For variable 'producttype' I used simple mapping by removing 'T' from the value and got the int value.
- Dropped unwanted data.

After this i was ready with my predictors and targets as :
<br />
||++++++++++++++++++++++++++++++++++++++++||+++++++++++||<br />
|| destinationbranch | originbranch | producttype|| overall_time||<br />
||++++++++++++++++++++++++++++++++++++++++||+++++++++++||<br />

Now i need to train the model by feeding it into the Neural Network model. I used Tensorflow to implement the NN regression model.

Defined the Hyperparameters and started the execution.

Execution went on for 3.5 hrs and I was ready with my model. I also wrote a small code to deploy it as a restful service using web.py library.

## The choice of algorithms and reasons for choosing them
I have previous experience with logistic regression and linear regression but every time Neural network models out performed them by giving lesser error rates. So decided to use Neural network regression model.
And also NN models can perform non-linear regression for higher degrees of freedom more efficiently as compared to Linear or Logistic regression.

## The actual code.

### File Descriptions
- corpus_handel.py : Data handler to manage text data and convert into numerical form.
- readjson.py : To read raw json data and perform feature engineering. Output will be the required variable as destinationbranch', 'originbranch' , 'producttype' and 'overall_time' in an excel file.
- train_model.py : Implementation of Neural Network regression model to perform time prediction.
- run_model.py : To execute the generated model.
- prediction_api.py : Deployement of model as REST service.

### Execution Steps:
- Performing Feature Engineering and pre-processing.
  - python readjson.py
- Train model
  - python train_model.py
- Execute model
  - python run_model.py

## Also comment on how you would make the prediction better, and what additional data you think will help you.
There are various ways by which we can make our model better as:
- Fine tunning the hyperparameters of the model.
- Introducing few more variabls in dataframe which can be relevant for the prediction
- Using RNN regression model. It can be helpful since it can compute more complex patters that a normal NN will.

Few datapoints that can be helpful are distance between Destination and Origin.
